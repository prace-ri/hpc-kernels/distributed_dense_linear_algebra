# DLA Interface

The use of ScaLAPACK libraries for numerical linear algebra is ubiquitous in high-performance computing applications,
and many vendors and software providers use it as the basis for their own high-performance math libraries.
However, a distributed GPU-enabled library doesn’t exist currently. Other packages for linear algebra with GPU support (or without GPU support but with better performances) exist,
but they have different interfaces, therefore major changes in the application code are needed to adopt them.

The DLA interface allows one to easily replace direct calls to ScaLAPACK routines. Its interface allows the user to decide which distributed linear algebra package to use at runtime.
It provides a C++ distributed matrix class that provides the possibility to convert the matrix layout
(e.g ScaLAPACK layout and DPLASMA tile layout) and wrappers to some of the distributed linear algebra routines.
It also provides a C and Fortran interface to the distributed linear algebra routines.
The DLA interface is compatible with ScaLAPACK, i.e. if a routine is not included in the interface the ScaLAPACK routine can be used easily.
(With the C and the Fortran interface this is straightforward, while the C++ matrix class provide a method that returns the ScaLAPACK descriptor needed for the ScaLAPACK calls.)

The interface is available in [Github](https://github.com/eth-cscs/DLA-interface)

## Distributed linear algebra libraries supported

The library supported are:
- ScaLAPACK (MKL, Libsci, Netlib, ...)
- ELPA
- D-Plasma (ParSEC)

## Documentation

[Documentation](https://github.com/eth-cscs/DLA-interface/tree/master/README.md)

## Install

[Install Guide](https://github.com/eth-cscs/DLA-interface/tree/master/INSTALL.md)

# Migration

The migration of an existing application from ScaLAPACK is easy following this steps: 

* Initialize the library (`dla_initialize` (C, Fortran), `comm::CommunicatorManager::initialize` (C++))
* Replace `(C)blacs_gridinit` calls with `dlai_create_2d_grid` (C, Fortran) or `comm::CommunicatorManager::createCommunicator2DGrid` (C++)
* Replace the ScaLAPACK calls with C or Fortran interface calls. 

# Examples

Examples of the different interfaces are available in the [miniapp](https://github.com/eth-cscs/DLA-interface/tree/master/miniapp) and [test/extra](https://github.com/eth-cscs/DLA-interface/tree/master/test/extra) directories and are built by the library building process.
The `--help` option provides the documentation of the command line parameters which specify the problem that has to be solved.


